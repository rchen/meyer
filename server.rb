require "bundler/setup"
require "sinatra"

set :server, 'webrick'

require "firebase"
require "google/api_client"
require "google/api_client/client_secrets"
require "google/api_client/auth/installed_app"
require "google_drive"

require "json"

GOOGLE_ISSUER = "843871274414-o2vsljbi8n18gvg22pvsl4oje0j116fc@developer.gserviceaccount.com"
SPREADSHEET_KEY = "0AqFz-V-xJpdvdFFwWlNWUUhGZG1wWkltWm43VHBwZnc"
FIREBASE_URI = "https://boiling-fire-9776.firebaseio.com/"

def retrieveToken
    client = Google::APIClient.new(
        :application_name => "Meyer",
        :application_version => "0.0.1"
    )
    key = Google::APIClient::KeyUtils.load_from_pkcs12('cert.p12', 'notasecret')
    client.authorization = Signet::OAuth2::Client.new(
        :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
        :audience => 'https://accounts.google.com/o/oauth2/token',
        :scope => 'https://www.googleapis.com/auth/prediction',
        :issuer => GOOGLE_ISSUER,
        :signing_key => key)
    client.authorization.scope = "https://www.googleapis.com/auth/drive " + "https://spreadsheets.google.com/feeds/"
    client.authorization.fetch_access_token!
    return client.authorization.access_token
end

def update_item(worksheet, sunet, identifier, value)

    session = GoogleDrive.login_with_oauth(retrieveToken)
    ws = session.spreadsheet_by_key(SPREADSHEET_KEY).worksheets[worksheet]

    # First search for the identifier column
    identifier_col = -1
    for col in 1..ws.num_cols
        if ws[1, col] == identifier
            identifier_col = col
            break
        end
    end

    # Next search for the sunet
    sunet_col = -1
    for row in 1..ws.num_rows
        if ws[row, 3] == "#{sunet}@stanford.edu"
            sunet_col = row
            break
        end
    end

    # Now update
    if identifier_col > 0 and sunet_col > 0
        ws[sunet_col, identifier_col] = value
        ws.save()
    end

end

get "/" do

    # Retrieve list of rubrics
    firebase = Firebase::Client.new(FIREBASE_URI)
    rubrics = firebase.get("rubrics").body

    # Start off with that data
    erb :index, :locals => { :rubrics => rubrics }

end

post "/:project/synchronize" do

    email = params[:email]
    payload = params[:payload]
    project = params[:project]

    # Retrieve the valid identifiers for cells
    firebase = Firebase::Client.new(FIREBASE_URI)
    rubric = firebase.get("rubrics/#{project}").body
    title = rubric["worksheet"]

    # Get the specific worksheet with Google Drive
    session = GoogleDrive.login_with_oauth(retrieveToken)
    worksheet = session.spreadsheet_by_key(SPREADSHEET_KEY).worksheet_by_title(title)

    # Find the row from the worksheet for the email
    row = -1
    for i in 1..worksheet.num_rows
        if worksheet[i, 3] == email
            row = i
            break
        end
    end

    # Go look for each key
    columns = []
    payload.keys.each do |key|
        rubric["questions"].each do |question|
            if question["name"] == key
                column = question["column"]
                for j in 1..worksheet.num_cols
                    if worksheet[1, j] == column
                        worksheet[row, j] = payload[key]
                        columns << { column: column, value: payload[key] }
                    end
                end   
            end
        end
    end

    worksheet.synchronize

    {email: email, columns: columns}.to_json

end

get "/:project/students" do

    project = params[:project]

    # Retrieve the identifier for the project
    firebase = Firebase::Client.new(FIREBASE_URI)
    worksheet_title = firebase.get("rubrics").body[project]["worksheet"]

    # Get the specific worksheet with Google Drive
    session = GoogleDrive.login_with_oauth(retrieveToken)
    worksheet = session.spreadsheet_by_key(SPREADSHEET_KEY).worksheet_by_title(worksheet_title)

    # Get the list of students, which is stored in column 3
    students = []
    for row in 2..worksheet.num_rows
        students << worksheet[row, 3]
    end

    # Return the list of students
    students.sort.to_json

end

get "/rubrics/:project" do
    project = params[:project]
    firebase = Firebase::Client.new(FIREBASE_URI)
    firebase.get("rubrics/#{project}").body.to_json
end

get "/worksheets" do

end

get "/:sunet/:identifier/:value" do
    sunet = params[:sunet]
    identifier = params[:identifier]
    value = params[:value]
    update_item(sunet, identifier, value)
end
